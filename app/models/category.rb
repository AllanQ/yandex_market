class Category < ActiveRecord::Base
  scope :main, -> { where(parent_id: 0) }
end
