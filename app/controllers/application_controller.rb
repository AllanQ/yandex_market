class ApplicationController < ActionController::Base
  before_action :main_menu_categories

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception







  private

  def main_menu_categories
    @categories = []
    Category.main.each do |main_category|
      sub_categories = []
        Category.where(parent_id: main_category.id).each do |sub_category|
        sub_categories << [sub_category.name, sub_category.id]
      end
      @categories << [[main_category.name, main_category.id], sub_categories]
    end
    top_menu
  end

  def top_menu
    @categories_top = @categories[0..8]
    @categories_top.each do |category|
      category[1] = category[1][0..8] if category[1].length > 8
    end
    # @categories_top << [['Ещё', 0], @categories[9..-1].transpose[0]]
  end
end
