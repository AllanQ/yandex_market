class CatalogController < ApplicationController
  before_action :catalog_menu



  private


  def catalog_menu
    @categories_catalog = @categories[0..11]
    @categories_catalog.each do |category|
      category[1] = category[1][0..6] if category[1].length > 8
    end
  end

end
