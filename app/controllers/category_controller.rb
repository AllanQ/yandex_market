class CategoryController < ApplicationController

  def show
    category_id    = params[:category_id]
    @category_name = params[:category_name]
    sub_categories_define(category_id)
  end

  private

  def sub_categories_define(category_id)
    @sub_categories = []
    Category.where(parent_id: category_id).each do |sub_category|
      sub_sub_categories = []
      Category.where(parent_id: sub_category.id).each do |sub_sub_category|
        sub_sub_categories << [sub_sub_category.name, sub_sub_category.id, sub_sub_category.electronics_like]
      end
      more_sub_sub_categories = nil
      if sub_sub_categories.length > 5
        more_sub_sub_categories = sub_sub_categories[5..-1]
        sub_sub_categories = sub_sub_categories[0..4]
      end
      @sub_categories << [[sub_category.name, sub_category.id, sub_category.electronics_like],
                          sub_sub_categories, more_sub_sub_categories]
    end
  end

end
