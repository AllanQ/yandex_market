class ClothesController < ApplicationController

  def index
    @category_id = params[:category_id]
    @products = Product.paginate(:page => params[:page], :per_page => 8).where(category_id: @category_id)
                    .select(:id, :name)
    products_ids = @products.map{ |p| p.id }
    @photos = Photo.where(product_id: products_ids, first: true).select(:product_id, :url).to_a
    attributes_name = AttributesName.find_by(category_id: @category_id, name: 'Цвет')
    @attributes_values = AttributesValue.where(product_id: products_ids, attributes_names_id: attributes_name)
                             .select(:product_id, :value).to_a
    @supply = Supply.where(product_id: products_ids).select(:product_id, :price).to_a
  end

end
